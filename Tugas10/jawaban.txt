Soal 1 Membuat Database

Soal 2 Membuat Table di Dalam Database

	categories
		CREATE TABLE categories(id int(8) auto_increment,name varchar(255),primary key(id));

	Items
        CREATE TABLE Items(id int(8) auto_increment,
            -> name varchar(255),
            -> description varchar (255),
            -> price int,
            -> stock int,
            -> category_id int(8),
            -> primary key(id),
            -> foreign key(category_id) references categories(id)
            -> );
    users
        CREATE TABLE users(id int auto_increment,
            -> name varchar(255),
            -> email varchar(255),
            -> password varchar(255),
            -> primary key(id)
            -> );

Soal 3 Memasukkan Data pada Table

    categories
     INSERT INTO categories (name)
    -> VALUE ("gadget"),("cloth"),("men"),("women"),("branded");


	Items
    INSERT INTO items (name, description, price, stock, category_id)
    -> VALUE("Sumsang b50","hape keren dari merek sumsang",4000000,100,1),
    -> ("Uniklooh","baju keren dari brand ternama",500000,50,2),
    -> ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);

    users
    INSERT INTO users (name, email, password)
    -> VALUE ("Jon Doe","john@doe.com","john123"),
    -> ("Jane Doe","jane@doe.com","jenita123");


Soal 4 Mengambil Data dari Database
	a. Mengambil data users
        SELECT id, name, email FROM users;

	b. Mengambil data items
        SELECT * FROM items WHERE price > 1000000;
        SELECT * FROM item WHERE name LIKE '%sang%';

	c. Menampilkan data items join dengan kategori
        SELECT items.name, items.description, items.price, items.stock, items.category_id, categories.name
        -> FROM items
        -> INNER JOIN categories ON item.category_id = categories.id;


Soal 5 Mengubah Data dari Database

   UPDATE item SET price = 2500000 WHERE name LIKE '%sang%';